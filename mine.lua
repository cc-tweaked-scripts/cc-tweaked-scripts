local function find_fuel()
    for i = 1, 16 do
        turtle.select(i)
        if turtle.refuel(0) then
            return true
        end
    end
    return false
end

local function find_pickaxe()
    for i = 1, 16 do
        turtle.select(i)
        if turtle.getItemDetail() ~= nil and string.find(turtle.getItemDetail().name, "pickaxe") then
            return true
        end
    end
    return false
end

local function check_health_async()
    while true do
        fuel_limit = turtle.getFuelLimit()
        fuel_level = turtle.getFuelLevel()
        if (fuel_level/fuel_limit < 0.1) then
            if find_fuel() then
                turtle.refuel(1)
            end
        end

        dig_success, dig_detail = turtle.dig("right")
        if not dig_success then
            if find_pickaxe() then
                turtle.equipRight()
            end
        end

        print("Finished health check")
        coroutine.yield()
    end
end

local function mine_async()
    while true do
        turtle.dig("right")
        turtle.forward()

        print("Finished mining")
        coroutine.yield()
    end
end

function main()
    parallel.waitForAny(check_health_async, mine_async)
end

main()
